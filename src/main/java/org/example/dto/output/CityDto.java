package org.example.dto.output;

import lombok.Data;

@Data
public class CityDto {
  private Long id;
  private String name;
  private Float latitude;
  private Float longitude;
  private CountryDto country;
}
