package org.example.dto.output;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class WeatherOutputDataDto {
  private Long id;

  private Float temperature;

  private Short pressure;

  private Short humidity;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime timeOfRecording;

  private CityDto city;
}
