package org.example.dto.output;

import lombok.Data;

@Data
public class CountryDto {
  private Short id;
  private String name;
}
