package org.example.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherInputDataDto {
  @Data
  public static class CoordinatesDto {
    @JsonProperty("lat")
    private Float latitude;

    @JsonProperty("lon")
    private Float longitude;
  }

  @Data
  public static class MainDto {
    private Float temp;
    private Short pressure;
    private Short humidity;
  }

  @JsonProperty("coord")
  private CoordinatesDto coordinates;

  private MainDto main;

  @JsonProperty("dt")
  private Long dateTimestamp;
}
