package org.example.mapper;

import lombok.Setter;
import org.example.dto.input.WeatherInputDataDto;
import org.example.dto.output.WeatherOutputDataDto;
import org.example.entity.WeatherData;
import org.example.service.CityService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
@Setter(onMethod_ = @Autowired)
public abstract class WeatherDataMapper {
  protected CityService cityService;
  protected CityMapper cityMapper;
  protected CountryMapper countryMapper;

  @Mapping(target = "id", ignore = true)
  @Mapping(
      target = "timeOfRecording",
      expression =
          "java(java.time.LocalDateTime.ofInstant(java.time.Instant.ofEpochSecond(weatherDataDto.getDateTimestamp()), java.util.TimeZone.getDefault().toZoneId()))")
  @Mapping(
      target = "city",
      expression =
          "java(cityService.findCityId(weatherDataDto.getCoordinates().getLatitude(), weatherDataDto.getCoordinates().getLongitude()))")
  @Mapping(target = "temperature", expression = "java(weatherDataDto.getMain().getTemp())")
  @Mapping(target = "pressure", expression = "java(weatherDataDto.getMain().getPressure())")
  @Mapping(target = "humidity", expression = "java(weatherDataDto.getMain().getHumidity())")
  public abstract WeatherData mapWeatherData(WeatherInputDataDto weatherDataDto);

  @Mapping(target = "id", source = "weatherData.id")
  @Mapping(target = "temperature", source = "weatherData.temperature")
  @Mapping(target = "humidity", source = "weatherData.humidity")
  @Mapping(target = "timeOfRecording", source = "weatherData.timeOfRecording")
  @Mapping(target = "city", expression = "java(cityMapper.mapCity(weatherData.getCity()))")
  public abstract WeatherOutputDataDto mapWeatherData(WeatherData weatherData);
}
