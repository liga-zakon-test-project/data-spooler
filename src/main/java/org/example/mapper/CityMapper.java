package org.example.mapper;

import lombok.Setter;
import org.example.dto.output.CityDto;
import org.example.entity.City;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
@Setter(onMethod_ = @Autowired)
public abstract class CityMapper {
  protected CountryMapper countryMapper;

  @Mapping(target = "id", source = "city.id")
  @Mapping(target = "name", source = "city.name")
  @Mapping(target = "latitude", source = "city.latitude")
  @Mapping(target = "longitude", source = "city.longitude")
  @Mapping(target = "country", expression = "java(countryMapper.mapCountry(city.getCountry()))")
  public abstract CityDto mapCity(City city);

  @Mapping(target = "id", source = "city.id")
  @Mapping(target = "name", source = "city.name")
  @Mapping(target = "latitude", source = "city.latitude")
  @Mapping(target = "longitude", source = "city.longitude")
  @Mapping(target = "country", expression = "java(countryMapper.mapCountry(city.getCountry()))")
  public abstract City mapCity(CityDto city);
}
