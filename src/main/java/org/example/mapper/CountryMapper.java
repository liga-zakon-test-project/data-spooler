package org.example.mapper;

import org.example.dto.output.CountryDto;
import org.example.entity.Country;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class CountryMapper {
  @Mapping(target = "id", source = "country.id")
  @Mapping(target = "name", source = "country.name")
  public abstract CountryDto mapCountry(Country country);

  @Mapping(target = "id", source = "country.id")
  @Mapping(target = "name", source = "country.name")
  public abstract Country mapCountry(CountryDto country);
}
