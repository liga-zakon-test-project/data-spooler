package org.example.repository;

import java.time.LocalDateTime;
import org.example.entity.WeatherData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeatherDataRepository extends JpaRepository<WeatherData, Long> {
  Page<WeatherData> findByCityNameIgnoreCase(String cityName, Pageable pageable);

  Page<WeatherData> findByCityId(Long cityId, Pageable pageable);

  Page<WeatherData> findByCityIdAndTimeOfRecordingBetween(
      Long cityId,
      LocalDateTime timeOfRecording1,
      LocalDateTime timeOfRecording2,
      Pageable pageable);
}
