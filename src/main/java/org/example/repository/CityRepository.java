package org.example.repository;

import java.util.Optional;
import org.example.entity.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
  @Query(
      "SELECT new org.example.entity.City(c.id) FROM City c WHERE ABS(c.latitude - :latitude) < 0.01 AND ABS(c.longitude - :longitude) < 0.01")
  Optional<City> findIdByLatitudeAndLongitude(Float latitude, Float longitude);

  Page<City> findByCountryId(Short countryId, Pageable pageable);

  Optional<City> findByNameIgnoreCase(String cityName);

  @Query("UPDATE City c SET c.name = :newName WHERE c.id = :id")
  @Modifying
  @Transactional
  int updateNameById(Long id, String newName);
}
