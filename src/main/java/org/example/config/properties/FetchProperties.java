package org.example.config.properties;

import java.util.List;
import lombok.Data;
import org.example.dto.input.WeatherInputDataDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "fetch")
@Data
public class FetchProperties {
  private String url;
  private List<WeatherInputDataDto.CoordinatesDto> coordinates;
}
