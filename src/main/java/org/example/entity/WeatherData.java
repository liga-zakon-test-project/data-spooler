package org.example.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import lombok.Data;

@Entity
@Table(name = "weather_data")
@Data
public class WeatherData {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @Column(name = "temperature", nullable = false)
  private Float temperature;

  @Column(name = "pressure", nullable = false)
  private Short pressure;

  @Column(name = "humidity", nullable = false)
  private Short humidity;

  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "city_id", nullable = false)
  private City city;

  @Column(name = "time_of_recording", nullable = false, unique = true)
  private LocalDateTime timeOfRecording;
}
