package org.example.entity;

import jakarta.persistence.*;
import java.util.Collection;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cities")
@Data
@NoArgsConstructor
public class City {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "id")
  private Long id;

  @Column(name = "name", unique = true, nullable = false, length = 128)
  private String name;

  @Column(name = "latitude", nullable = false)
  private Float latitude;

  @Column(name = "longitude", nullable = false)
  private Float longitude;

  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "country_id", nullable = false)
  private Country country;

  @OneToMany(
      mappedBy = "city",
      cascade = {CascadeType.REMOVE},
      orphanRemoval = true)
  private Collection<WeatherData> weatherData;

  public City(Long id) {
    this.id = id;
  }

  public City(String name) {
    this.name = name;
  }

  public City(String name, Float latitude, Float longitude, Country country) {
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.country = country;
  }
}
