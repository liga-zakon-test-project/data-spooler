package org.example.entity;

import jakarta.persistence.*;
import java.util.Collection;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "countries")
@Data
@NoArgsConstructor
public class Country {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "id")
  private Short id;

  @Column(name = "name", unique = true, nullable = false, length = 128)
  private String name;

  @ToString.Exclude
  @OneToMany(
      mappedBy = "country",
      cascade = {CascadeType.REMOVE})
  private Collection<City> cities;
}
