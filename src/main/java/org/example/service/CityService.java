package org.example.service;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.example.dto.output.CityDto;
import org.example.entity.City;
import org.example.entity.Country;
import org.example.mapper.CityMapper;
import org.example.repository.CityRepository;
import org.example.repository.CountryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CityService {
  private final CityRepository cityRepository;
  private final CountryRepository countryRepository;
  private final CityMapper cityMapper;

  public City findCityId(Float latitude, Float longitude) {
    return cityRepository.findIdByLatitudeAndLongitude(latitude, longitude).orElseThrow();
  }

  public Page<CityDto> findCityByCountryId(
      short countryId,
      int pageNumber,
      int itemsPerPage,
      boolean descending,
      String... sortByFields) {
    return cityRepository
        .findByCountryId(
            countryId,
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending ? Sort.Direction.DESC : Sort.Direction.ASC,
                sortByFields))
        .map(cityMapper::mapCity);
  }

  public Page<CityDto> findAll(
      int pageNumber, int itemsPerPage, boolean descending, String... sortByFields) {
    return cityRepository
        .findAll(
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending ? Sort.Direction.DESC : Sort.Direction.ASC,
                sortByFields))
        .map(cityMapper::mapCity);
  }

  public CityDto findCityById(long cityId) {
    Optional<City> optionalCity = cityRepository.findById(cityId);
    City city = optionalCity.orElseThrow();
    return cityMapper.mapCity(city);
  }

  public CityDto findCityByName(String cityName) {
    Optional<City> optionalCity = cityRepository.findByNameIgnoreCase(cityName);
    City city = optionalCity.orElse(new City("No city found"));
    return cityMapper.mapCity(city);
  }

  @Transactional
  public CityDto createCity(CityDto createFrom) {
    City city = cityMapper.mapCity(createFrom);
    Country country = countryRepository.findById(city.getCountry().getId()).orElseThrow();
    country.getCities().add(city);
    return cityMapper.mapCity(cityRepository.save(city));
  }

  public void updateCityName(long cityId, String newName) {
    cityRepository.updateNameById(cityId, newName);
  }
}
