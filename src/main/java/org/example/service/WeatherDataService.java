package org.example.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.output.WeatherOutputDataDto;
import org.example.entity.City;
import org.example.entity.WeatherData;
import org.example.mapper.CityMapper;
import org.example.mapper.CountryMapper;
import org.example.mapper.WeatherDataMapper;
import org.example.repository.CityRepository;
import org.example.repository.WeatherDataRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Data
@Slf4j
public class WeatherDataService {
  private final WeatherDataRepository weatherDataRepository;
  private final CityRepository cityRepository;
  private final WeatherDataMapper weatherDataMapper;
  private final CityMapper cityMapper;
  private final CountryMapper countryMapper;

  public WeatherData save(WeatherData weatherData) {
    return weatherDataRepository.save(weatherData);
  }

  @Transactional
  public List<WeatherData> save(Iterable<WeatherData> weatherData) {
    Set<Long> cityIds =
        StreamSupport.stream(weatherData.spliterator(), false)
            .map(wd -> wd.getCity().getId())
            .collect(Collectors.toUnmodifiableSet());
    List<City> cities = cityRepository.findAllById(cityIds);
    cities.forEach(
        city ->
            city.getWeatherData()
                .addAll(
                    StreamSupport.stream(weatherData.spliterator(), false)
                        .filter(wd -> wd.getCity().getId().equals(city.getId()))
                        .toList()));
    List<WeatherData> savedWeatherData = weatherDataRepository.saveAll(weatherData);
    log.info("Saved {} WeatherData records", savedWeatherData.size());
    return savedWeatherData;
  }

  public Page<WeatherOutputDataDto> findByCityNameAndDatesBetween(
      long cityId,
      LocalDateTime date1,
      LocalDateTime date2,
      int pageNumber,
      int itemsPerPage,
      boolean descending,
      String... sortByFields) {
    return weatherDataRepository
        .findByCityIdAndTimeOfRecordingBetween(
            cityId,
            date1,
            date2,
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending ? Sort.Direction.DESC : Sort.Direction.ASC,
                sortByFields))
        .map(weatherDataMapper::mapWeatherData);
  }

  public Page<WeatherOutputDataDto> findAll(
      int pageNumber, int itemsPerPage, boolean descending, String... sortByFields) {
    return weatherDataRepository
        .findAll(
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending ? Sort.Direction.DESC : Sort.Direction.ASC,
                sortByFields))
        .map(weatherDataMapper::mapWeatherData);
  }

  public Page<WeatherOutputDataDto> findByCityId(
      long cityId, int pageNumber, int itemsPerPage, boolean descending, String... sortByFields) {
    return weatherDataRepository
        .findByCityId(
            cityId,
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending
                    ? Sort.by(sortByFields).descending()
                    : Sort.by(sortByFields).ascending()))
        .map(weatherDataMapper::mapWeatherData);
  }
}
