package org.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.output.CountryDto;
import org.example.entity.Country;
import org.example.mapper.CountryMapper;
import org.example.repository.CountryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CountryService {
  private final CountryRepository countryRepository;
  private final CountryMapper countryMapper;

  public Page<CountryDto> findAll(
      int pageNumber, int itemsPerPage, boolean descending, String... sortByFields) {
    return countryRepository
        .findAll(
            PageRequest.of(
                pageNumber,
                itemsPerPage,
                descending
                    ? Sort.by(sortByFields).descending()
                    : Sort.by(sortByFields).ascending()))
        .map(countryMapper::mapCountry);
  }

  public CountryDto createCountry(CountryDto countryToCreate) {
    Country country = countryMapper.mapCountry(countryToCreate);
    log.info("Saving new country with name {}", country.getName());
    return countryMapper.mapCountry(countryRepository.save(country));
  }

  public void deleteCountry(short countryId) {
    countryRepository.deleteById(countryId);
  }
}
