package org.example.scheduler;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import lombok.Data;
import org.example.config.properties.FetchProperties;
import org.example.dto.input.WeatherInputDataDto;
import org.example.entity.WeatherData;
import org.example.mapper.WeatherDataMapper;
import org.example.service.CityService;
import org.example.service.WeatherDataService;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@Data
public class WeatherDataFetcher {
  private final WeatherDataService weatherDataService;
  private final CityService cityService;
  private final WeatherDataMapper weatherDataMapper;
  private final FetchProperties fetchProperties;
  private final WebClient webClient;

  @Scheduled(fixedDelay = 30000)
  public void fetchData() {
    List<Mono<WeatherInputDataDto>> monoList =
        fetchProperties.getCoordinates().stream().map(this::sendRequest).toList();
    List<WeatherInputDataDto> weatherInputDataDtos = new ArrayList<>();

    Mono.when(monoList)
        .materialize()
        .subscribe(
            voidSignal -> {
              if (!voidSignal.isOnError()) {
                List<WeatherData> weatherDataToSave =
                    monoList.stream()
                        .map(
                            weatherDataDtoMono -> {
                              try {
                                return weatherDataDtoMono.toFuture().get();
                              } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                              } catch (ExecutionException e) {
                                throw new RuntimeException(e);
                              }
                            })
                        .toList()
                        .stream()
                        .map(weatherDataMapper::mapWeatherData)
                        .toList();
                weatherDataService.save(weatherDataToSave);
              }
            });
  }

  private Mono<WeatherInputDataDto> sendRequest(WeatherInputDataDto.CoordinatesDto coordinates) {
    return webClient
        .get()
        .uri(
            fetchProperties.getUrl(),
            uriBuilder ->
                uriBuilder
                    .queryParam("appid", "8c63c138bb57c91690f63326aa8ee044")
                    .queryParam("lat", coordinates.getLatitude())
                    .queryParam("lon", coordinates.getLongitude())
                    .build())
        .accept(MediaType.APPLICATION_JSON)
        .acceptCharset(StandardCharsets.UTF_8)
        .retrieve()
        .bodyToMono(WeatherInputDataDto.class);
  }
}
