package org.example.controller;

import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.example.dto.output.CityDto;
import org.example.service.CityService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/cities")
@RequiredArgsConstructor
public class CityController {
  private final CityService cityService;

  @GetMapping("/all")
  public Mono<Page<CityDto>> getCitiesByCountryId(
      @RequestParam(name = "countryId", defaultValue = "0") short countryId,
      @RequestParam(defaultValue = "0") int pageNumber,
      @RequestParam(defaultValue = "100") int itemsPerPage) {
    return Mono.defer(
        () ->
            countryId > 0
                ? Mono.just(
                    cityService.findCityByCountryId(
                        countryId, pageNumber, itemsPerPage, false, "name"))
                : Mono.just(cityService.findAll(pageNumber, itemsPerPage, false, "name")));
  }

  @PutMapping("/add")
  public Mono<ResponseEntity<CityDto>> createCity(@RequestBody CityDto city) {
    return Mono.defer(
        () -> {
          try {
            return Mono.just(
                new ResponseEntity<>(cityService.createCity(city), HttpStatus.CREATED));
          } catch (NoSuchElementException e) {
            return Mono.just(new ResponseEntity<>(HttpStatus.NOT_FOUND));
          }
        });
  }

  @GetMapping("/{cityId}")
  public Mono<ResponseEntity<?>> getCityById(@PathVariable long cityId) {
    return Mono.defer(
        () -> {
          try {
            return Mono.just(ResponseEntity.ok((cityService.findCityById(cityId))));
          } catch (NoSuchElementException e) {
            return Mono.just(new ResponseEntity<>(HttpStatus.NOT_FOUND));
          }
        });
  }

  @PatchMapping("/{cityId}/editName")
  public Mono<ResponseEntity<?>> changeCityName(
      @PathVariable long cityId, @RequestBody String newName) {
    return Mono.defer(
        () -> {
          cityService.updateCityName(cityId, newName);
          return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        });
  }

  @GetMapping("{cityName}")
  public Mono<CityDto> getCityByName(@PathVariable String cityName) {
    return Mono.defer(() -> Mono.just(cityService.findCityByName(cityName)));
  }
}
