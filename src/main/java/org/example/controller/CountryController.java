package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.dto.output.CountryDto;
import org.example.service.CountryService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/countries")
@RequiredArgsConstructor
public class CountryController {
  private final CountryService countryService;

  @GetMapping("/all")
  public Mono<Page<CountryDto>> getCountries(
      @RequestParam(defaultValue = "0") int pageNumber,
      @RequestParam(defaultValue = "100") int itemsPerPage) {
    return Mono.defer(
        () -> Mono.just(countryService.findAll(pageNumber, itemsPerPage, false, "name")));
  }

  @PutMapping("/add")
  public Mono<ResponseEntity<CountryDto>> addCountry(@RequestBody CountryDto country) {
    return Mono.defer(
        () ->
            Mono.just(
                new ResponseEntity<>(countryService.createCountry(country), HttpStatus.CREATED)));
  }

  @DeleteMapping("/remove")
  public Mono<ResponseEntity<?>> removeCountry(@RequestParam(name = "id") short countryId) {
    return Mono.defer(
        () -> {
          countryService.deleteCountry(countryId);
          return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        });
  }
}
