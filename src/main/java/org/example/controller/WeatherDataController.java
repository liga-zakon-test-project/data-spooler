package org.example.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;
import lombok.RequiredArgsConstructor;
import org.example.dto.output.WeatherOutputDataDto;
import org.example.service.WeatherDataService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/weatherData")
@RequiredArgsConstructor
public class WeatherDataController {
  private final WeatherDataService weatherDataService;

  @GetMapping("/all")
  public Mono<Page<WeatherOutputDataDto>> getWeatherDataByCityId(
      @RequestParam(name = "cityId", defaultValue = "0") long cityId,
      @RequestParam(defaultValue = "0") int pageNumber,
      @RequestParam(defaultValue = "100") int itemsPerPage,
      @RequestParam(defaultValue = "true") boolean descending) {
    return Mono.defer(
        () ->
            cityId == 0
                ? Mono.just(
                    weatherDataService.findAll(
                        pageNumber, itemsPerPage, descending, "timeOfRecording"))
                : Mono.just(
                    weatherDataService.findByCityId(
                        cityId, pageNumber, itemsPerPage, descending, "timeOfRecording")));
  }

  @GetMapping("/betweenDates")
  public Mono<Page<WeatherOutputDataDto>> getWeatherDataByCityAndDatesBetween(
      @RequestParam(name = "cityId") long cityId,
      @RequestParam(name = "date1") long date1,
      @RequestParam(name = "date2") long date2,
      @RequestParam(defaultValue = "0") int pageNumber,
      @RequestParam(defaultValue = "100") int itemsPerPage,
      @RequestParam(defaultValue = "false") boolean descending) {
    return Mono.defer(
        () -> {
          LocalDateTime localDateTime1 =
              LocalDateTime.ofInstant(
                  Instant.ofEpochMilli(date1), TimeZone.getDefault().toZoneId());
          LocalDateTime localDateTime2 =
              LocalDateTime.ofInstant(
                  Instant.ofEpochMilli(date2), TimeZone.getDefault().toZoneId());
          return Mono.just(
              weatherDataService.findByCityNameAndDatesBetween(
                  cityId,
                  localDateTime1,
                  localDateTime2,
                  pageNumber,
                  itemsPerPage,
                  descending,
                  "timeOfRecording"));
        });
  }
}
